const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
    comment: {
        type: String,
        required: true
    },
    author:{
        type: String,
        required: true
    },
    filename:{
        type: String,
        required: true
    },
    date:{
        type:Date,
        default: Date.now
    }
});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;