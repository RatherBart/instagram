const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const multer = require('multer');


const User = require('../models/User');

// const storage = multer.diskStorage({
//     destination: function(req,file, cb){
//         cb(null, './uploads');
//     },
//     filename: (req,file,cb) =>{
//         cb(null, file.originalname);
//     }
// });

// const upload = multer({storage: storage});



router.get('/login', (req, res) => res.render('login'));

router.get('/register', (req, res) => res.render('register'));

router.post('/register', (req,res)=> {
   const {name,nickname,email,password,password2} = req.body;
   let errors = [];
   if(!name || !email || !password || !password2 || !nickname){
       errors.push({msg:'fill everything'});
   }

   if(password != password2){
       errors.push({msg: 'passwords dont match'});
   }

   if(errors.length > 0){
       res.render('register', {
           errors,
           name,
           nickname,
           email,
           password,
           password2,
       });
   } else {
        User.findOne({nickname: nickname})
            .then(user =>{
                if(user){
                    errors.push({msg : 'nickname already registered'})
                    res.render('register', {
                        errors,
                        name,
                        nickname,
                        email,
                        password,
                        password2,
                    });
                }else{
                    const newUser = new User({
                        name,
                        nickname,
                        email,
                        password,
                    });
                    bcrypt.genSalt(10, (err, salt) => 
                        bcrypt.hash(newUser.password, salt, (err,hash) =>{
                            if(err) throw err;
                            console.log(newUser);
                            newUser.password = hash;
                            newUser.save()
                                .then(user => {
                                    req.flash('successmsg','already registerd and can login');
                                    res.redirect('login');
                                })
                                .catch(err =>console.log(err));
                    } ))
                }
            });
   }
});

//loghandle

router.post('/login',(req,res,next)=>{
    passport.authenticate('local',{
        successRedirect: '/dashboard',
        failureRedirect: '/users/login',
        failureFlash: true
    })(req,res,next);
});

//logouthandle

router.get('/logout', (req,res) => {
    req.logout();
    req.flash('successmsg', 'logged out');
    res.redirect('/users/login');
});

module.exports = router ;