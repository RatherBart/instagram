const express = require('express');
const router = express.Router();
const {ensureAuth} = require ('../config/auth');
const GridFsStorage = require('multer-gridfs-storage');
const mongoose = require('mongoose');
const User = require('../models/User');
const Comment = require('../models/Comment');
const multer = require('multer');
const crypto = require('crypto');
const Grid = require('gridfs-stream');

const path = require('path');

const database = require('../config/keys').MongoURI; 
const conn = mongoose.connection;
let gfs;
conn.once('open', ()=>{
    gfs = Grid(conn.db,mongoose.mongo);
    gfs.collection('uploads');
})
 
var storage = new GridFsStorage({
    url: database,
    file: (req, file) => {
      return new Promise((resolve, reject) => {
        crypto.randomBytes(16, (err, buf) => {
          if (err) {
            return reject(err);
          }
          const photoowner = req.user.nickname;

          const filename = buf.toString('hex') + path.extname(file.originalname);
          const fileInfo = {
            filename: filename,
            bucketName: 'uploads',
            aliases:{owner: photoowner}
          };
          resolve(fileInfo);
        });
      });
    }
  });
const upload = multer({ storage });


router.get('/', (req, res) => res.render('welcome') );

router.get('/dashboard', ensureAuth, (req, res) =>{

    gfs.files.find().toArray((err, files)=>{
        if(!files || files.length ===0){
            res.render('dashboard',{
                name: req.user.name,
                desc:false,
                files:false,
                nickname:req.user.nickname
            });
        } else{
            files.map(file => {
                if( file.contentType === 'image/jpeg' || file.contentType ==='image/png'){
                    if(file.aliases.owner === req.user.nickname){
                        file.isImage = true;
                        console.log(file.aliases.owner);
                        if(file.aliases.profile) {
                            file.isProfile = true;
                        }
                    }
                }else{
                    file.isImage = false;
                }
            });
            res.render('dashboard',{
                files:files,
                name: req.user.name,
                desc: req.user.desc,
                nickname: req.user.nickname,
                profile: req.user.profile
            }); 
        }
    });
});


router.get('/edit', (req,res)=> {
    res.render('edit',{
        user:req.user
    });
});

router.post('/edit', upload.single('profile'),(req,res)=>{
    
    User.updateOne({email: req.user.email},{
        $set:{
            desc: req.body.desc
        }
    }, function(err,resp){
        console.log(resp);
    });
    if(typeof req.file.filename !== undefined){
        User.updateOne({email: req.user.email},{
            $set:{
                profile:req.file.filename
            }
        }, function(err,resp){
            console.log(resp);
        });
        console.log(req.file.filename);
        res.redirect('dashboard'); 
    }      
});

router.get('/photos', (req,res) => {
    res.render('photos', {
        nickname: req.user.nickname
    });
});

router.post('/photos', upload.single('photos'), (req,res) => {
    console.log('added photo');
    res.redirect('dashboard');
});

router.get('/profiles', ensureAuth, (req,res) => {
    User.find()
    .then(users =>{
       res.render('profiles',{
            files:users,
            nickname:req.user.nickname
        }); 
    });
});
router.get('/images/:filename', (req,res) => {
    gfs.files.findOne({filename: req.params.filename}, (err,file)=>{
        if(!file ||file.length ===0){
            return res.status(404).json({
                err: 'no file'
            });
        }
        if(file.contentType === 'image/jpeg'|| file.contentType === 'image/png'){
            const readstream = gfs.createReadStream(file.filename);
            readstream.pipe(res);
        }else{
            res.status(404).json({
                err: 'no an image'
            });
        }
    });
});

router.get('/photo/:photo', (req,res)=>{
    const photopng = req.params.photo+".png";
    const photojpg = req.params.photo+".jpg";
    gfs.files.find({filename:photopng}).toArray((err, files)=>{
        if(!files || files.length ===0){
            gfs.files.find({filename:photojpg}).toArray((err, files)=>{
                if(!files || files.length ===0){
                    res.status(404).json({
                        err: 'no an image'
                    });
                } else{
                    Comment.find({filename:photojpg})
                    .then(comments =>{
                        if(comments){
                            gfs.files.find().toArray((err, photos)=>{
                                if(!photos || photos.length ===0){
                                    res.render('dashboard',{
                                        name: req.user.name,
                                        profilephoto: req.user.profilephoto,
                                        photos:false
                                    });
                                } else{
                                    photos.map(file => {
                                        if( file.contentType === 'image/jpeg' || file.contentType ==='image/png'){
                                            
                                                file.isImage = true;
                                        }else{
                                            file.isImage = false;
                                        }
                                    });
    
                                    User.find()
                                        .then(users =>{
                                           res.render('photo',{
                                                photos:photos,
                                                users:users,
                                                nickname:req.user.nickname,
                                                comments:comments,
                                                files:files,
                                                user:req.user,
                                            }); 
                                        });
                                }
                            });  
                        }else{
                            User.find()
                                .then(users =>{
                                    res.render('photo',{
                                        files:files,
                                        comments:false,
                                        user:req.user,
                                    }); 
                                });
                        }
                    });
                }
            });
        } else{
            Comment.find({filename:photopng})
                .then(comments =>{
                    if(comments){
                        gfs.files.find().toArray((err, photos)=>{
                            if(!photos || photos.length ===0){
                                res.render('dashboard',{
                                    name: req.user.name,
                                    profilephoto: req.user.profilephoto,
                                    photos:false
                                });
                            } else{
                                photos.map(file => {
                                    if( file.contentType === 'image/jpeg' || file.contentType ==='image/png'){
                                        
                                            file.isImage = true;
                                    }else{
                                        file.isImage = false;
                                    }
                                });

                                User.find()
                                    .then(users =>{
                                       res.render('photo',{
                                            photos:photos,
                                            users:users,
                                            nickname:req.user.nickname,
                                            comments:comments,
                                            files:files,
                                            user:req.user,
                                        }); 
                                    })
                            }
                        });      
                    }else{
                        Comment.find({filename:photojpg})
                            .then(comments =>{
                                if(comments){
                                    gfs.files.find().toArray((err, photos)=>{
                                        if(!photos || photos.length ===0){
                                            res.render('dashboard',{
                                                name: req.user.name,
                                                profilephoto: req.user.profilephoto,
                                                photos:false
                                            });
                                        } else{
                                            photos.map(file => {
                                                if( file.contentType === 'image/jpeg' || file.contentType ==='image/png'){
                                                    
                                                        file.isImage = true;
                                                }else{
                                                    file.isImage = false;
                                                }
                                            });
            
                                            User.find()
                                                .then(users =>{
                                                   res.render('photo',{
                                                        photos:photos,
                                                        users:users,
                                                        nickname:req.user.nickname,
                                                        comments:comments,
                                                        files:files,
                                                        user:req.user,
                                                    }); 
                                                })
                                        }
                                    });  
                                }else{
                                    User.find()
                                        .then(users =>{
                                            res.render('photo',{
                                                files:files,
                                                comments:false,
                                                user:req.user,
                                            }); 
                                        })
                                }
                            });
                    }
                });
        }
    });
});

router.post('/photo/:photo', (req,res)=> {
    const filename =req.params.photo;
    const url = filename.split('.',1);
    const author = req.user.email;
    const comment = req.body.comment;
    console.log(filename);
    console.log(comment);
    console.log(author);
    const newComment = new Comment({
        comment,
        author,
        filename
    });
    newComment.save()
        .then(user => {
            res.redirect('/photo/'+url);
        })
        .catch(err =>console.log(err));
    
});
    
router.get('/:nickname',ensureAuth,(req, res) => {
    const site = req.params.nickname;
    User.findOne({nickname:site})
        .then(user =>{
            if(user){
                gfs.files.find().toArray((err, files)=>{
                    if(!files || files.length ===0){
                        res.render('usersite',{
                            name: req.user.name,
                            profilephoto: req.user.profilephoto,
                            files:false,
                            nickname:req.user.nickname,
                            desc: req.user.desc
                        });
                    } else{
                        files.map(file => {
                            if( file.contentType === 'image/jpeg' || file.contentType ==='image/png'){
                                if(file.aliases.owner === req.user.nickname){
                                    file.isImage = true;
                                    console.log(file.aliases.owner);
                                    if(file.aliases.profile) {
                                        file.isProfile = true;
                                    }
                                }
                            }else{
                                file.isImage = false;
                            }
                        });
                        res.render('usersite',{
                            files:files,
                            name: req.user.name,
                            desc: req.user.desc,
                            nickname: req.user.nickname,
                            profile: req.user.profile
                        }); 
                    }
                });
            }
        })
});



module.exports = router;