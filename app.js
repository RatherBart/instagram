const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const GridFsStorage = require('gridfs-stream');
const Grid = require('gridfs-stream');
const methodOverride = require('method-override');
const passport = require('passport');
const bodyParser = require('body-parser');
const crypto = require('crypto');
const path = require('path');
const multer = require('multer');

const app = express();


//passport
require('./config/passport')(passport);

app.use(bodyParser.json());
app.use(methodOverride('_method'));

const database = require('./config/keys').MongoURI; 
mongoose.connect(database, {useNewUrlParser:true})
    .then(() => {
        console.log('connected to db..');
    })
    .catch(err => console.log(err));


//ejs
app.use(expressLayouts);
app.set('view engine', 'ejs');
app.use("/uploads", express.static(__dirname + '/uploads'));

app.use(bodyParser.json());

app.use(express.urlencoded({extended:false}));
    
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
}))
//passportmiddleware
app.use(passport.initialize());
app.use(passport.session());

//flashconnect
app.use(flash());
app.use((req,res,next) =>{
    res.locals.successmsg = req.flash('successmsg');
    res.locals.errormsg = req.flash('errormsg');
    res.locals.error = req.flash('error');
    next();
});

//routes
app.use('/', require('./routes/index'));

app.use('/users', require('./routes/users'));
app.use('/*', require('./routes/index'));

//route test

const PORT = process.env.PORT || 5000;
app.listen(PORT, console.log(`server started on port ${PORT}`));